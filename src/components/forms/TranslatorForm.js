import React, { useState } from 'react';
import { FormControl, InputGroup } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Translator from '../translator/Translator'
import Profile from '../profile/Profile';

const TranslatorForm = () => {

  const [word, setword] = useState('');


  const onChangeInput = event => {
    setword(event.target.value);
  }

  return (
    <div>
      <Link to="/profile">Profile</Link>


      <div className="translatorForm">
        <InputGroup className="mb-3">
          <FormControl
            placeholder="Enter word or short sentence"
            onChange={onChangeInput}
          />
        </InputGroup>
      </div>
      <Translator letterprop={word} />
    </div>
  );
}
export default TranslatorForm;