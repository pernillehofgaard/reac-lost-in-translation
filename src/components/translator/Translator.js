import React from 'react';
import { Button, Jumbotron } from 'react-bootstrap';
import './Translator.css';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getWordsByIdAction, setWordsAction } from '../../store/actions/wordsActions';

const Translator = (props) => {

  const words = useSelector(state => state.words);
  const dispatch = useDispatch();

  const [letters, setLetters] = useState([]);

  const onTranslateClick = event => {
    //set lettes to empty so the new word won't add to the previous
    setLetters([]);

    const prop = props.letterprop.toLowerCase();
    const word = prop.split("");

    word.forEach((letter, index) => {
      setLetters(oldLetters => [
        ...oldLetters,
        {
          id: index,
          letter: letter
        }
      ]);
    });

    console.log(letters);

    let wordsArr = [];
    wordsArr.push(prop);
    console.log(prop);
    console.log("wordsArr:");
    console.log(wordsArr);

    dispatch(setWordsAction(wordsArr))
  }

  return (
    <div className="translator">
      <Button variant="primary" onClick={onTranslateClick}>Translate</Button>
      <Jumbotron className="jumbotron">
        <h1 id="translate"></h1>

        <div className="signImages">
          <ul className="translatorUl">
            {
            
            letters.map(l => (
              <li key={l.id} className="translatorIl">
                <h1>{l.letter}</h1>
                {(l.letter === ' ') ? <p className="spaceInPic"></p> : <img src={require(`../../images/${l.letter}.png`)} alt="sign" height="75px" />}
              </li>
            ))

            }
          </ul>
        </div>
      </Jumbotron>



    </div>
  )
}

export default Translator;