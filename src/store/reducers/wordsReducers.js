import { ACTION_SET_WORDS, ACTION_GET_WORD_BY_ID } from '../actions/wordsActions'

export function wordsReducer(state = [], action){

  switch(action.type){
    case ACTION_SET_WORDS:
      return [...state, ...action.payload];
    case ACTION_GET_WORD_BY_ID:
      return state.find(word => word.id === action.payload)
    default:
      return state;
  }

}