import React from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { setWordsAction } from '../../store/actions/wordsActions';


const Profile = () =>{

  const searchedWords = useSelector(state => state.words
    .slice(state.words.length-10, state.words.length).reverse());

  console.log(searchedWords);

  
  
  

  return(
    <div>
      <Link to="/translator">Back to Translator</Link>

      <h3>Your last searched words</h3>
      <ul>
        {searchedWords.map(word => (
          <li key={word.id}>{word}</li>
        ))}
      </ul>
    </div>
  )
}

export default Profile;