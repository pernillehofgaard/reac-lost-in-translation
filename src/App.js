import React from 'react';
import './App.css';

import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
//my components
import LoginForm from './components/forms/LoginForm';
import TranslatorForm from './components/forms/TranslatorForm';
import Header from'./components/header/Header';
import Profile from './components/profile/Profile'

function App() {

  return (
    <Router>
      <Header/>
      <div className="App">
        <Switch>
          <Route exact path="/" component={LoginForm} />
          <Route path="/translator" component={TranslatorForm}/>
          <Route path="/profile" component={Profile} />
        </Switch>
        
      </div>
    </Router>

  );
}

export default App;
