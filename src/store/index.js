import { createStore, combineReducers} from 'redux';
import { wordsReducer } from './reducers/wordsReducers'

const rootReducers = combineReducers({
  words: wordsReducer
})
const store = createStore(
  rootReducers,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

export default store;

