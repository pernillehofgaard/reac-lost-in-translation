import React, { useState } from 'react';
import Button from 'react-bootstrap/Button';
import { Form, Jumbotron } from 'react-bootstrap';
import './Forms.css';
import { setStorage, getStorage } from '../../utils/localStorage'

const LoginForm = props => {

  const [username, setUsername] = useState('');

  
  if(getStorage('username_session')){
    props.history.push('/translator');
  }
  

  const onSubmitClick = event => {
    console.log("Event: ", event.target);
    console.log("Username: ", username);
    console.log(props.history);

    setStorage('username_session', {
      session:{
        username
      }
    });

    props.history.push('/translator');
  }

  const onUsernameChange = event => {
    setUsername(event.target.value);
    
  }

  return (
    <div>
      <Jumbotron className="loginJumbotron">
        <h3>Login</h3>
        <Form.Group>
          <div className="loginform">
            <div>
              <label>Username: </label>
              <Form.Control type="text" placeholder="Enter username" onChange={onUsernameChange} />
            </div>

            <div className="submitBtn">
              <Button variant="primary" onClick={onSubmitClick}>Submit</Button>
            </div>
          </div>
        </Form.Group>
      </Jumbotron>
    </div>
  );
};

export default LoginForm;