export const ACTION_SET_WORDS = 'ACTION_SET_WORDS';
export const ACTION_GET_WORD_BY_ID = 'ACTION_GET_WORD_BY_ID';

export const setWordsAction = (words = []) =>({
  type: ACTION_SET_WORDS,
  payload: words
});

export const getWordsByIdAction = wordId =>({
  type: ACTION_GET_WORD_BY_ID,
  payload: wordId
});
